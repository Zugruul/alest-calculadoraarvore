package alest;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Calculadora {
	public static void main (String[] args) {
		
		ArrayList<String> expressoes = new ArrayList<>(); // Guarda Express�es
		ArrayList<ExpressionTree> arvores = new ArrayList<>(); // Guarda �rvores das Express�es
		String fileName = "expressoes.txt";
		
		try {
			carregaExpressoes(expressoes, fileName);
		} catch (IOException e) {
			System.err.println("N�o foi poss�vel carregar arquivo de expressoes. Mensagem de erro: \n" + e.getMessage());
		}
		
		int count = 1;
		for(String s : expressoes) {
			System.out.format("Express�o %s: %s%n", count, s);
			ExpressionTree et = new ExpressionTree();
			et.gerarDe(s);
			arvores.add(et);
			System.out.format("Root da Express�o %s:  %s%n%n", count, et.root);
			count++;
		}
		
		System.out.println("### Status das �rvores ###");
		count = 0;
		for(ExpressionTree et : arvores) {
			String status = "ok";
			if(et.root == null) status = "not ok";
			System.out.format("ExpressionTree - et_id: %s - %s - Express�o: %s%n", count, status, expressoes.get(count));
			count++;
		}
		
int n = 0;
		
		// Mostrar �rvore
		Scanner sc = new Scanner(System.in);
		while ((n >= 0) && (n < arvores.size())) {
			ExpressionTree et = arvores.get(n);
			if(et.root != null) {
				System.out.println("\n\n### Dados ET ###");
				System.out.format("Express�o: %s%n", expressoes.get(n));
				System.out.format("Express�o sem espa�os: %s%n", expressoes.get(n).replaceAll(" ", ""));
				System.out.format("Raiz da �rvore: %s%n", et.getRoot());
				System.out.format("N�mero de nodos: %s%n", et.count());
				System.out.format("Altura da �rvore: %s%n", et.height());
				System.out.format("Resposta da express�o: %s%n", et.calcularResposta()); // TEM DE FAZER ISSO
				System.out.format("M�dia dos valores: %s%n", et.calcularMedia());
				System.out.format("N�mero de nodos folha: %s%n", et.countFolhas());
				System.out.format("Caminhamento Pr�: %s%n", et.positionsPre()); // pr� com express�o 1 [ok]
				System.out.format("Caminhamento P�s: %s%n", et.positionsPos()); // pos com express�o 1 [ok] // � o caminhamento para resolver a equa��o, usa recurs�o
				System.out.format("Caminhamento Central: %s%n", et.positionsCentral()); // central com express�o 1 [ok]
				System.out.format("Caminhamento Largura: %s%n", et.positionsWidth()); // largura com express�o 1 [ok]
			} else {
				System.err.println("�rvore Inv�lida. ");
			}
			System.out.format("\n\nDigite um n�mero negativo ou maior que %s para sair: ", arvores.size()-1);
			n = sc.nextInt();
		}
		sc.close();
		System.out.println("\nFim da execu��o!");
		
	}
	
	
	private static void carregaExpressoes(ArrayList<String> expressoes, String fileName) throws IOException {
		Path file = Paths.get(fileName);
		try (Scanner sc = new Scanner(Files.newBufferedReader(file, Charset.forName("utf8")))) {
		  sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
		  while (sc.hasNextLine()) {
		    String exp = sc.nextLine().replaceAll("(\r)|(\t)|(\n)", "");
		    if (exp.startsWith("\uFEFF")) { // tem um caractere de encoding no inicio do arquivo
		    	exp = exp.substring(1); 
		    } 
		    expressoes.add(exp);
		  }
		}
		catch (IOException x) {
		  System.err.format("Erro de E/S: %s%n", x);
		}
	}
}
