package alest;

import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("unused")
public class ExpressionTree {
	public class Node {
		public String element;
		public Node father;
		public Node left;
		public Node right;
		boolean complete;

		public Node() {
			element = null;
			father = null;
			left = null;
			right = null;
			complete = false;
		}

		public Node(String element) {
			this.element = element;
			father = null;
			left = null;
			right = null;
			complete = false;
		}

		public String toString() {
			String s = "[Esquerda = ";
			if (left != null)
				s += left.element;
			s += ", Raiz = " + element + ", Direita = ";
			if (right != null)
				s += right.element;
			s += "]";
			return s;
		}
	}

	public Node root;

	/*
	 * Gerar �rvore
	 */

	public void gerarDe(String expressao) {
		String[] vetor_expressao = expressao.split(" ");

		int parenteses_abertos = expressao.split("\\(", -1).length;
		int parenteses_fechados = expressao.split("\\)", -1).length;
		if (parenteses_abertos != parenteses_fechados) {
			System.err.format(
					"Express�o com erro. N�mero de parenteses abertos incompat�vel com numero de parenteses fechados. (Express�o: %s)%n",
					expressao);
			return;
		}

		ArrayList<Node> nodes = new ArrayList<>();
		for (String s : vetor_expressao) {
			add(s, nodes);
		}
		if (nodes.size() > 0) {
			root = nodes.get(0);
		} else {
			System.err.println("N�o foi poss�vel gerar �rvore da express�o");
		}
	}

	public void add(String element, ArrayList<Node> pre_arvore) {
		if (element.equals("(")) {
			pre_arvore.add(new Node());
			System.err.println("New Node");
		} else if (isOperator(element)) {
			System.err.println("Operador " + element);
			Node aux = findLastNotCompleteWithoutElement(pre_arvore);
			if (aux != null)
				aux.element = element;
			if (findNodeIndex(aux, pre_arvore) > 0) {
				Node father = pre_arvore.get(findNodeIndex(aux, pre_arvore) - 1);
				if (father != null)
					aux.father = father;
			}

		} else if (isNumber(element) != null) {
			System.err.println("Number " + element);
			Node aux = findLastNotComplete(pre_arvore);
			// verifica se tem raiz
			if (aux.element != null) {// se tiver raiz adiciona na direita
				aux.right = new Node(element); // cria nodo cujo elemento
				aux.right.father = aux;
			} else {// se n�o tiver raiz adiciona na esquerda
				if (aux.left == null) { // talvez aqui o problema esteja em
					aux.left = new Node(element);
					aux.left.father = aux;
				} else {
					if (aux.right == null) {
						aux.right = new Node(element);
						aux.right.father = aux;
					} else {
						// System.err.println("ERRO 1");
					}
				}
			}
		} else if (element.equals(")")) {
			System.err.println("Fechando nodo em aberto");
			checkCompleted(pre_arvore);
			Node aux = findLastNotComplete(pre_arvore);
			Node completo = findLastCompleted(pre_arvore);
			if (aux != null)
				if (aux.element == null && aux.left == null) {
					aux.left = completo;
					completo.father = aux;
					pre_arvore.remove(completo);
					checkCompleted(pre_arvore);
				} else// at� aqui ok
				if (aux.right == null && aux != null) {
					aux.right = completo;
					completo.father = aux;
					pre_arvore.remove(completo);
					checkCompleted(pre_arvore);
				} else if (aux.left == null && aux != null) {
					aux.left = completo;
					completo.father = aux;
					pre_arvore.remove(completo);
					checkCompleted(pre_arvore);
				} else {
					System.err.println("Verificar if statement. N�o foi possivel alocar em lugar algum");
				}
		}
	}

	public int findNodeIndex(Node n, ArrayList<Node> pre_arvore) {
		for (int i = 0; i < pre_arvore.size(); i++) {
			if (pre_arvore.get(i) == n) {
				return i;
			}
		}
		return -1;
	}

	public Node findLastCompleted(ArrayList<Node> pre_arvore) {
		Node last = null;
		if (!(pre_arvore.size() > 0)) {
			System.out.println("Vazio");
			return null;
		}
		for (int i = pre_arvore.size() - 1; i >= 0; i--) {
			Node aux = pre_arvore.get(i);
			if (aux.complete) {
				last = aux;
				break;
			}
		}
		return last;
	}

	public Node findLastNotComplete(ArrayList<Node> pre_arvore) {
		Node last = null;
		if (!(pre_arvore.size() > 0)) {
			System.out.println("Vazio");
			return null;
		}
		for (int i = pre_arvore.size() - 1; i >= 0; i--) {
			Node aux = pre_arvore.get(i);
			if (!aux.complete) {
				last = aux;
				break;
			}
		}
		return last;
	}

	public Node findLastNotCompleteWithoutElement(ArrayList<Node> pre_arvore) {
		Node last = null;
		if (!(pre_arvore.size() > 0)) {
			System.out.println("Vazio");
			return null;
		}
		for (int i = pre_arvore.size() - 1; i >= 0; i--) {
			Node aux = pre_arvore.get(i);
			if (!aux.complete && aux.element == null) {
				last = aux;
				break;
			}
		}
		return last;
	}

	public Node findLastNotCompleteWithElement(ArrayList<Node> pre_arvore) {
		Node last = null;
		if (!(pre_arvore.size() > 0)) {
			System.out.println("Vazio");
			return null;
		}
		for (int i = pre_arvore.size() - 1; i >= 0; i--) {
			Node aux = pre_arvore.get(i);
			if (!aux.complete && aux.element != null) {
				last = aux;
				break;
			}
		}
		return last;
	}

	public boolean isOperator(String s) {
		switch (s) {
		case "+":
		case "-":
		case "/":
		case "*":
		case "^":
			return true;
		default:
			return false;
		}
	}

	public boolean isOperator(char s) {
		switch (s) {
		case '+':
		case '-':
		case '/':
		case '*':
		case '^':
			return true;
		default:
			return false;
		}
	}

	public Integer isNumber(String s) {
		Integer i = null;
		try {
			i = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return null;
		}
		return i;
	}

	public Integer getIndex(Node n, ArrayList<Node> pre_arvore) {
		for (int i = 0; i < pre_arvore.size(); i++) {
			if (pre_arvore.get(i) == n) {
				return i;
			}
		}
		return null;
	}

	public void checkCompleted(ArrayList<Node> pre_arvore) {
		for (Node n : pre_arvore) {
			if (!n.complete) {
				if (n.element != null && n.left != null && n.right != null) {
					n.complete = true;
				}
			}
		}
	}

	// Caminhamentos
	public LinkedListOfString positionsPre() {
		if (root == null) {
			System.err.format("N�o h� raiz. Portanto n�o � pos�vel fazer caminhamento.\n");
			return null;
		}
		LinkedListOfString res = new LinkedListOfString();
		positionsPreAux(root, res);
		return res;
	}

	private void positionsPreAux(Node n, LinkedListOfString res) {
		if (n != null) {
			res.add(n.element); // Visita o nodo
			positionsPreAux(n.left, res); // Visita a subarvore esquerda
			positionsPreAux(n.right, res); // Visita a subarvore direita
		}
	}

	public LinkedListOfString positionsPos() {
		if (root == null) {
			System.err.format("N�o h� raiz. Portanto n�o � pos�vel fazer caminhamento.\n");
			return null;
		}
		LinkedListOfString res = new LinkedListOfString();
		positionsPosAux(root, res);
		return res;
	}

	private void positionsPosAux(Node n, LinkedListOfString res) {
		if (n != null) {
			positionsPosAux(n.left, res); // Visita a subarvore esquerda
			positionsPosAux(n.right, res); // Visita a subarvore direita
			res.add(n.element); // Visita o nodo
		}
	}

	public LinkedListOfString positionsCentral() {
		if (root == null) {
			System.err.format("N�o h� raiz. Portanto n�o � pos�vel fazer caminhamento.\n");
			return null;
		}
		LinkedListOfString res = new LinkedListOfString();
		positionsCentralAux(root, res);
		return res;
	}

	private void positionsCentralAux(Node n, LinkedListOfString res) {
		if (n != null) {
			positionsCentralAux(n.left, res); // Visita a subarvore esquerda
			res.add(n.element); // Visita o nodo
			positionsCentralAux(n.right, res); // Visita a subarvore direita
		}
	}

	public double calcularResposta() {
		if (root == null) {
			System.err.format("N�o h� raiz. Portanto n�o � pos�vel fazer caminhamento.\n");
			return -1;
		}

		double resposta = 0;
		resposta = calcularRespostaAux(root, resposta);
		return resposta;
	}
	
	private double calcularRespostaAux(Node n, double resposta) {
		if (n != null) {
			switch (n.element) {
			case "+":
				resposta += calcularRespostaAux(n.left, resposta) + calcularRespostaAux(n.right, resposta);
				break;
				
			case "-":
				resposta += calcularRespostaAux(n.left, resposta) - calcularRespostaAux(n.right, resposta);
				break;
				
			case "*":
				resposta += calcularRespostaAux(n.left, resposta) * calcularRespostaAux(n.right, resposta);
				break;
				
			case "/":
				resposta += calcularRespostaAux(n.left, resposta) / calcularRespostaAux(n.right, resposta);
				break;
				
			case "^":
				resposta += Math.pow(calcularRespostaAux(n.left, resposta), calcularRespostaAux(n.right, resposta));
				break;
				
			default:
				return Double.parseDouble(n.element);
				
			}
		}
		
		return resposta;
	}

	public LinkedListOfString positionsWidth() {
		if (root == null) {
			System.err.format("N�o h� raiz. Portanto n�o � pos�vel fazer caminhamento.\n");
			return null;
		}
		Queue<Node> fila = new Queue<>();
		LinkedListOfString res = new LinkedListOfString();
		fila.enqueue(root);
		auxPositionsWidth(fila, res);
		return res;
	}

	public void auxPositionsWidth(Queue<Node> fila, LinkedListOfString res) {
		if (fila.size() <= 0)
			return;
		Node n = fila.dequeue();
		res.add(n.element);
		if (n.left != null)
			fila.enqueue(n.left);
		if (n.right != null)
			fila.enqueue(n.right);
		auxPositionsWidth(fila, res);
	}

	public int height() {
		return findHeight(root);
	}

	int findHeight(Node n) {
		if (n == null)
			return 0;
		return 1 + Math.max(findHeight(n.left), findHeight(n.right));
	}

	public ExpressionTree copiarArvore() { // Quest�o 10
		ExpressionTree copia = new ExpressionTree();
		copia.root = copiarNodo(root);
		return copia;
	}

	public Node copiarNodo(Node node) {
		Node copia = new Node(node.element);
		if (node.left != null) {
			copia.left = copiarNodo(node.left);
		}
		if (node.right != null) {
			copia.right = copiarNodo(node.right);
		}
		return copia;
	}

	public int countFolhas() {
		return auxCountFolhas(root);
	}

	public int auxCountFolhas(Node n) {
		if (n == null)
			return 0;
		if (n.left == null && n.right == null)
			return 1;
		return auxCountFolhas(n.left) + auxCountFolhas(n.right);
	}
	
	public double somarValores() {
		return auxSomarValores(root);
	}

	public double auxSomarValores(Node n) {
		if (n == null)
			return 0;
		if (n.left == null && n.right == null)
			return Double.parseDouble(n.element);
		
		 return auxSomarValores(n.left) + auxSomarValores(n.right);
	}
	
	public double calcularMedia() {
		return somarValores() / countFolhas();
	}

	private void remove(Node n) {
		Node pai;
		pai = n.father;

		// Remocao de nodo folha
		if (n.left == null && n.right == null) {
			if (root == n) { // remocao do unico elemento da arvore
				root = null;
				return;
			}
			if (pai.right == n)
				pai.right = null;
			else
				pai.left = null;
		}

		// Remocao de esquerda nula e direita nao
		if (n.left == null && n.right != null) {
			if (root == n) {
				root = n.right;
				return;
			}
			if (pai.left == n) {
				pai.left = n.right;
			} else {
				pai.right = n.right;
			}
			n.right.father = pai;
			return;
		}

		// Remocao de direita nula e esquerda nao
		if (n.left != null && n.right == null) {
			if (root == n) {
				root = n.left;
				return;
			}
			if (pai.left == n) {
				pai.left = n.left;
			} else {
				pai.right = n.left;
			}
			n.left.father = pai;
			return;
		}
	}

	private Node searchNodeRef(String element, Node n) {
		if (n == null || element == null) {
			return null;
		}

		int c = n.element.compareTo(element);

		if (c == 0) { // se forem iguais...
			return n; // ...achou!
		}
		if (c > 0) {
			return searchNodeRef(element, n.left);
		} else {
			return searchNodeRef(element, n.right);
		}
	}

	public int count() {
		return count(root);
	}

	// Conta o numero de nodos a partir da subarvore cuja referencia
	// para a raiz esta sendo passada por parametro
	private int count(Node n) { // importante
		if (n == null)
			return 0;
		else
			return 1 + count(n.left) + count(n.right);
	}

	public void clear() {
		root = null;
	}

	public boolean isEmpty() {
		return (root == null);
	}

	public String getRoot() {
		if (isEmpty()) {
			throw new EmptyTreeException();
		}
		return root.element;
	}
}
